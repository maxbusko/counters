package max.busko.counters;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NumberActivity extends AppCompatActivity {

    @Bind(R.id.number_layout)
    TextInputLayout mNumberLayout;
    @Bind(R.id.number)
    TextInputEditText mNumberView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_number);
        ButterKnife.bind(this);

        mNumberView.setText(Prefs.getString("number", ""));
        mNumberView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mNumberLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.next)
    public void onNextClick(View view){
        String number = mNumberView.getText().toString();
        if(number.isEmpty()){
            mNumberLayout.setError(getString(R.string.number_error));
        }else{
            Intent intent = new Intent(NumberActivity.this, MainActivity.class);
            intent.putExtra("number", number);
            startActivity(intent);
        }
    }
}
