package max.busko.counters;


import max.busko.counters.model.Counters;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CountersApi {
    @GET("/test.php")
    Call<Counters> getCountersData(@Query("account") String account);
}
