
package max.busko.counters.model;

import com.google.gson.annotations.SerializedName;

public class Counter {

    @SerializedName("id")
    private Integer id;
    @SerializedName("number")
    private String number;
    @SerializedName("client_id")
    private Integer clientId;
    @SerializedName("client_address")
    private String clientAddress;
    @SerializedName("service_id")
    private Integer serviceId;
    @SerializedName("service_name")
    private String serviceName;
    @SerializedName("evidence")
    private Integer evidence;
    @SerializedName("evidence_past")
    private Integer evidencePast;
    @SerializedName("unit")
    private String unit;
    @SerializedName("is_closed")
    private Boolean isClosed;
    @SerializedName("date_audit")
    private Object dateAudit;
    @SerializedName("date_close")
    private Object dateClose;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public Integer getEvidence() {
        return evidence;
    }

    public void setEvidence(Integer evidence) {
        this.evidence = evidence;
    }

    public Integer getEvidencePast() {
        return evidencePast;
    }

    public void setEvidencePast(Integer evidencePast) {
        this.evidencePast = evidencePast;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Boolean getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(Boolean isClosed) {
        this.isClosed = isClosed;
    }

    public Object getDateAudit() {
        return dateAudit;
    }

    public void setDateAudit(Object dateAudit) {
        this.dateAudit = dateAudit;
    }

    public Object getDateClose() {
        return dateClose;
    }

    public void setDateClose(Object dateClose) {
        this.dateClose = dateClose;
    }

}
