
package max.busko.counters.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Counters {

    @SerializedName("info")
    private List<Info> info = null;
    @SerializedName("counters")
    private List<Counter> counters = null;

    public List<Info> getInfo() {
        return info;
    }

    public void setInfo(List<Info> info) {
        this.info = info;
    }

    public List<Counter> getCounters() {
        return counters;
    }

    public void setCounters(List<Counter> counters) {
        this.counters = counters;
    }

}
