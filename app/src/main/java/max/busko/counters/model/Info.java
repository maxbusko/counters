
package max.busko.counters.model;

import com.google.gson.annotations.SerializedName;

public class Info {

    @SerializedName("country_name")
    private String countryName;
    @SerializedName("region_name")
    private String regionName;
    @SerializedName("municipal_name")
    private Object municipalName;
    @SerializedName("city_name")
    private String cityName;
    @SerializedName("district_name")
    private String districtName;
    @SerializedName("street_name")
    private String streetName;
    @SerializedName("company_name")
    private String companyName;
    @SerializedName("home_house")
    private String homeHouse;
    @SerializedName("home_housing")
    private Object homeHousing;
    @SerializedName("home_name")
    private String homeName;
    @SerializedName("home_address")
    private String homeAddress;
    @SerializedName("flat_number")
    private String flatNumber;
    @SerializedName("flat_name")
    private String flatName;
    @SerializedName("flat_address")
    private String flatAddress;
    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;
    @SerializedName("room_number")
    private Object roomNumber;
    @SerializedName("room")
    private Object room;
    @SerializedName("address")
    private String address;
    @SerializedName("count_counters")
    private Integer countCounters;
    @SerializedName("count_counters_closed")
    private Integer countCountersClosed;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public Object getMunicipalName() {
        return municipalName;
    }

    public void setMunicipalName(Object municipalName) {
        this.municipalName = municipalName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getHomeHouse() {
        return homeHouse;
    }

    public void setHomeHouse(String homeHouse) {
        this.homeHouse = homeHouse;
    }

    public Object getHomeHousing() {
        return homeHousing;
    }

    public void setHomeHousing(Object homeHousing) {
        this.homeHousing = homeHousing;
    }

    public String getHomeName() {
        return homeName;
    }

    public void setHomeName(String homeName) {
        this.homeName = homeName;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    public String getFlatName() {
        return flatName;
    }

    public void setFlatName(String flatName) {
        this.flatName = flatName;
    }

    public String getFlatAddress() {
        return flatAddress;
    }

    public void setFlatAddress(String flatAddress) {
        this.flatAddress = flatAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(Object roomNumber) {
        this.roomNumber = roomNumber;
    }

    public Object getRoom() {
        return room;
    }

    public void setRoom(Object room) {
        this.room = room;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCountCounters() {
        return countCounters;
    }

    public void setCountCounters(Integer countCounters) {
        this.countCounters = countCounters;
    }

    public Integer getCountCountersClosed() {
        return countCountersClosed;
    }

    public void setCountCountersClosed(Integer countCountersClosed) {
        this.countCountersClosed = countCountersClosed;
    }

}
