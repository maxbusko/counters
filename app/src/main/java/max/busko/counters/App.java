package max.busko.counters;

import android.app.Application;
import android.content.ContextWrapper;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class App extends Application {

    private CountersApi mCountersApi;

    @Override
    public void onCreate() {
        super.onCreate();

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(Prefs.NAME)
                .setUseDefaultSharedPreference(false)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://dl.erpico.ru/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mCountersApi = retrofit.create(CountersApi.class);
    }

    public CountersApi getCountersApi(){
        return mCountersApi;
    }
}
