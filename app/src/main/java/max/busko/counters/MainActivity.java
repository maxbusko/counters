package max.busko.counters;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonParseException;

import java.net.UnknownHostException;

import butterknife.Bind;
import butterknife.ButterKnife;
import max.busko.counters.model.Counters;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.recycler)
    RecyclerView mRecyclerView;
    @Bind(R.id.progressbar)
    ProgressBar mProgressBar;

    private String mAccountNumber;
    private Call<Counters> mCountersCall;

    private DialogInterface.OnClickListener mNumberErrorListener;
    private DialogInterface.OnClickListener mConnectionErrorListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        mNumberErrorListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity.this.onBackPressed();
            }
        };

        mConnectionErrorListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getCountersData();
            }
        };

        mAccountNumber = getIntent().getStringExtra("number");
        mCountersCall = ((App) getApplication()).getCountersApi().getCountersData(mAccountNumber);

        getCountersData();
    }

    private void getCountersData() {
        mProgressBar.setVisibility(View.VISIBLE);
        mCountersCall.enqueue(new Callback<Counters>() {
            @Override
            public void onResponse(Call<Counters> call, retrofit2.Response<Counters> response) {
                mProgressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.body().getCounters() != null && response.body().getInfo() != null) {

                    Prefs.putString("number", mAccountNumber);

                    mRecyclerView.setAdapter(new CountersAdapter(response.body()));
                } else {
                    showErrorDialog(getString(R.string.error_check_number), android.R.string.ok, mNumberErrorListener);
                }
            }

            @Override
            public void onFailure(Call<Counters> call, Throwable t) {
                mProgressBar.setVisibility(View.GONE);

                if (t instanceof UnknownHostException) {
                    showErrorDialog(getString(R.string.error_connection), R.string.try_again, mConnectionErrorListener);
                } else if (t instanceof JsonParseException) {
                    showErrorDialog(getString(R.string.error_check_number), android.R.string.ok, mNumberErrorListener);
                } else {
                    showErrorDialog(t.getMessage(), android.R.string.ok, null);
                }
            }
        });
    }

    private void showErrorDialog(String message, int button, DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle(getString(R.string.error))
                .setMessage(message);

        if (listener != null) {
            builder.setPositiveButton(button, listener);
        }
        builder.show();
    }
}
