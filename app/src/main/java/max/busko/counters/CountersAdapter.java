package max.busko.counters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import max.busko.counters.model.Counter;
import max.busko.counters.model.Counters;
import max.busko.counters.model.Info;

public class CountersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int TYPE_HEADER = 0;
    private final int TYPE_COUNTER = 1;

    private Counters mCountersData;

    public CountersAdapter(Counters data) {
        mCountersData = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            view = inflater.inflate(R.layout.adapter_item_header, parent, false);
            return new HeaderViewHolder(view);
        } else {
            view = inflater.inflate(R.layout.adapter_item_counter, parent, false);
            return new CounterViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_HEADER) {
            Info info = mCountersData.getInfo().get(0);
            HeaderViewHolder headerViewHolder = (HeaderViewHolder)holder;
            headerViewHolder.mAccount.setText(String.valueOf(info.getId()));
            headerViewHolder.mAddress.setText(info.getAddress());
        } else {
            Counter item = mCountersData.getCounters().get(position - 1);
            CounterViewHolder counterViewHolder = (CounterViewHolder) holder;
            counterViewHolder.mCounterName.setText(item.getServiceName());
            counterViewHolder.mCounterNumber.setText(item.getNumber());
            counterViewHolder.mPrevValue.setText(String.valueOf(item.getEvidencePast()));
            counterViewHolder.mCurValue.setText(String.valueOf(item.getEvidence()));
        }
    }

    @Override
    public int getItemCount() {
        return mCountersData.getCounters().size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_COUNTER;
        }
    }

    public class CounterViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.name)
        TextView mCounterName;
        @Bind(R.id.number)
        TextView mCounterNumber;
        @Bind(R.id.prev_value)
        TextView mPrevValue;
        @Bind(R.id.cur_value)
        TextView mCurValue;

        public CounterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.account)
        TextView mAccount;
        @Bind(R.id.address)
        TextView mAddress;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
